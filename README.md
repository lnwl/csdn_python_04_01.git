# csdn_python_04_01

#### 项目介绍
csdn学院python全栈第四模块第一章作业

#### 文件说明
youao.py为有道翻译程序
58.py为58同城房源信息抓取程序
maoyan.py为猫眼top100榜单信息抓取程序

#### 完成情况
有道翻译完成输入中英文自动返回翻译结果功能，完成urllib和requests两种请求方式，由于有道翻译提交数据中salt及sign值需进行算法计算方可得出，又因本人js水平着实不高，
故在网上找了一篇关于有道翻译的算法的文章来参考，文章地址:https://blog.csdn.net/weixin_43248813/article/details/82820699
感谢博主的热心分享

58同城完成了指定页码页面和指定几页信息爬取功能，默认从第一页获取，未写自定义第几页到第几页获取，后期有时间会优化
完成了爬取的信息保存到文件功能，并且每次爬取保存文件名均不同，避免了爬取信息堆积导致的凌乱

猫眼top100爬取程序完成了top100榜单的信息爬取并保存到文件，并且每次文件名均不同

有道翻译使用urllib和requests两种请求方式，58和猫眼均使用requests方式请求


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
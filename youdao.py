import urllib.request
import urllib.parse
import json
import time
import random
import hashlib
import requests

'''
urllib方式取自网上一篇文章，其中涉及js相关知识，由于本人对此不是很熟练，所以照搬过来的，js部分为提交信息中的salt和sign值的计算
文章链接https://blog.csdn.net/weixin_43248813/article/details/82820699
感谢博主的分享
'''


def translate_urllib(content):
    url = 'http://fanyi.youdao.com/translate_o?smartresult=dict&smartresult=rule'

    # 定义变量
    client = 'fanyideskweb'
    ctime = int(time.time() * 1000)
    salt = str(ctime + random.randint(1, 10))

    # 这是秘钥
    key = '6x(ZHw]mwzX#u0V7@yfwK'
    sign = hashlib.md5((client + content + salt + key).encode('utf-8')).hexdigest()

    # 表单数据
    data = {
        "i": content,
        "from": "AUTO",
        "to": "AUTO",
        "smartresult": "dict",
        "client": "fanyideskweb",
        "salt": salt,
        "sign": sign,
        "doctype": "json",
        "version": "2.1",
        "keyfrom": "fanyi.web",
        "action": "FY_BY_REALTIME",
        "typoResult": "false",
    }
    data = urllib.parse.urlencode(data).encode('utf-8')

    # 请求头
    head = {
        "Accept": "application/json, text/javascript, */*; q=0.01",
        "Accept-Language": "zh-CN,zh;q=0.9",
        "Cookie": "OUTFOX_SEARCH_USER_ID=1907361188@10.168.8.76; OUTFOX_SEARCH_USER_ID_NCOO=2090942704.858357;"
                  " _ntes_nnid=a669543bbbcb736318dfcaf6378c8ab0,1540220687737; P_INFO=yn4535fsq06; ANTICSRF=cleared;"
                  " NTES_OSESS=cleared; S_OINFO=; JSESSIONID=aaacyXdlJKRJW5sZk13Bw; fanyi-ad-id=52077;"
                  " fanyi-ad-closed=1; ___rl__test__cookies=" + str(ctime),
        "Host": "fanyi.youdao.com",
        "Connection": "keep-alive",

        # 这个不要添加到报头，否则一直是errorcode=50
        # "Upgrade-Insecure-Requests": "1",

        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko)"
                      " Chrome/70.0.3538.77 Safari/537.36",

        "Origin": "http://fanyi.youdao.com",
        "X-Requested-With": "XMLHttpRequest",
        "Referer": "http://fanyi.youdao.com/",
    }

    # 构造请求对象
    request = urllib.request.Request(url, data, headers=head)
    # 发送请求
    response = urllib.request.urlopen(request)
    # 接码返回数据
    response = response.read().decode("utf8")
    # print(response)
    target = json.loads(response)
    result = target['translateResult'][0][0]['tgt']
    '''
    此处是取出词的解释 但是在中译英的时候 可能会出现获取不到导致异常
    for translate in target['smartResult']['entries']:
        print(translate)
    '''
    return result


def translate_requests(content):
    url = 'http://fanyi.youdao.com/translate_o?smartresult=dict&smartresult=rule'

    # 定义变量
    client = 'fanyideskweb'
    ctime = int(time.time() * 1000)
    salt = str(ctime + random.randint(1, 10))

    # 这是秘钥
    key = '6x(ZHw]mwzX#u0V7@yfwK'
    sign = hashlib.md5((client + content + salt + key).encode('utf-8')).hexdigest()

    # 表单数据
    data = {
        "i": content,
        "from": "AUTO",
        "to": "AUTO",
        "smartresult": "dict",
        "client": "fanyideskweb",
        "salt": salt,
        "sign": sign,
        "doctype": "json",
        "version": "2.1",
        "keyfrom": "fanyi.web",
        "action": "FY_BY_REALTIME",
        "typoResult": "false",
    }
    # data = urllib.parse.urlencode(data).encode('utf-8')

    # 请求头
    head = {
        "Accept": "application/json, text/javascript, */*; q=0.01",
        "Accept-Language": "zh-CN,zh;q=0.9",
        "Cookie": "OUTFOX_SEARCH_USER_ID=1907361188@10.168.8.76; OUTFOX_SEARCH_USER_ID_NCOO=2090942704.858357;"
                  " _ntes_nnid=a669543bbbcb736318dfcaf6378c8ab0,1540220687737; P_INFO=yn4535fsq06; ANTICSRF=cleared;"
                  " NTES_OSESS=cleared; S_OINFO=; JSESSIONID=aaacyXdlJKRJW5sZk13Bw; fanyi-ad-id=52077;"
                  " fanyi-ad-closed=1; ___rl__test__cookies=" + str(ctime),
        "Host": "fanyi.youdao.com",
        "Connection": "keep-alive",

        # 这个不要添加到报头，否则一直是errorcode=50
        # "Upgrade-Insecure-Requests": "1",

        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko)"
                      " Chrome/70.0.3538.77 Safari/537.36",

        "Origin": "http://fanyi.youdao.com",
        "X-Requested-With": "XMLHttpRequest",
        "Referer": "http://fanyi.youdao.com/",
    }

    # request = urllib.request.Request(url, data, headers=head)
    # response = urllib.request.urlopen(request)
    # 发送请求
    response = requests.post(url, data=data, headers=head)
    # print(response)
    # 解析数据
    target = json.loads(response.text)
    result = target['translateResult'][0][0]['tgt']
    '''
    此处是取出词的解释 但是在中译英的时候 可能会出现获取不到导致异常
    for translate in target['smartResult']['entries']:
        print(translate)
    '''
    return result


if __name__ == '__main__':
    while True:
        tran_sys = input('请选择翻译系统:1.urllib 2.requests 3.退出')
        if tran_sys == '1':
            while True:
                print('当前翻译系统:urllib')
                content = input('请输入需要翻译的内容(q返回选择系统)：')
                if content == 'q':
                    break
                else:
                    print(translate_urllib(content))

        elif tran_sys == '2':
            while True:
                print('当前翻译系统:requests')
                content = input('请输入需要翻译的内容(q返回选择系统)：')
                if content == 'q':
                    break
                else:
                    print(translate_requests(content))
        elif tran_sys == '3':
            print('欢迎再次使用，再见')
            break
        else:
            print('翻译系统选择错误,请重新选择')

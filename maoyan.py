import requests
import re
import time


def get_maoyan():
    datas = []
    for i in range(0, 10):
        url = 'http://maoyan.com/board/4?offset=' + str(i * 10)
        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/70.0.3538.77 Safari/537.36'}
        res = requests.get(url, headers=headers)
        html = res.text.replace('\n', '').replace('\t', '').replace(' ', '')
        # print(res.text)
        # print(html)
        pat = '<dd>(.*?)</dd>'
        data = re.findall(pat, html)
        if i == 0:
            datas = data
        else:
            datas += data

    pat2 = '<iclass="board-indexboard-index-.*?">(.*?)</i>.*?"title="(.*?)".*?<imgdata-src="(.*?)"alt=".*?主演：(.*?)' \
           '</p>.*?上映时间：(.*?)</p>.*?<iclass="integer">(.*?)</i><iclass="fraction">(.*?)</i>'
    a = 0
    # 以追加写模式打开文件 并以maoyan_+时间戳命名 确保每次抓取数据保存到不同文件中
    text = open('maoyan_' + str(time.time()) + '.txt', mode='a+')
    for i in datas:
        results = re.findall(pat2, i)
        if len(results) == 0:
            # print(i)
            pass
        else:
            if False:
                pass
            else:
                info = '-' * 20 + '第' + str(a + 1) + '条数据' + '-' * 20
                info1 = '序号:' + results[0][0] + "\n图片链接:" + results[0][2] + "\n电影名称:" + results[0][1] + "\n主演:" + \
                        results[0][3] + '\n时间:' + results[0][4] + "\n评分:" + results[0][5] + results[0][6]
                text.write(info + '\n')
                text.write(info1 + '\n')
                print(info)
                print(info1)
                # print('-' * 20 + '第' + str(a + 1) + '条数据' + '-' * 20)
                a += 1
    info2 = '-' * 20 + '统计信息' + '-' * 20
    info3 = '抓取时间:'+str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) )+'\n抓取结果:共抓取' + str(len(datas)) + "条数据,符合要求数据共计" + str(a) + "条"
    text.write(info2 + '\n')
    text.write(info3 + '\n')
    print(info2)
    print(info3)
    text.close()


if __name__ == '__main__':
    print('欢迎使用猫眼电影Top100榜单信息抓取程序')
    while True:
        input_sys = input('请输入操作指令: 1.开始抓取Top100榜单信息 2.退出')
        if input_sys == '1':
            get_maoyan()
        elif input_sys == '2':
            print('感谢使用，再见!')
            break
        else:
            print('请输入正确指令')

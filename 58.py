import requests
import re
import time
'''
获取58房源信息by张宇
本程序使用requests进行请求
本程序爬取过程中使用了两次正则匹配来避免数据过大导致程序响应时间过长
如爬取不到数据请打开浏览器访问http://bj.58.com/dashanzi/chuzu/pn1/?ClickID=1 手动点击验证码进行验证
'''


def get_58(page):
    url = 'http://bj.58.com/dashanzi/chuzu/pn' + str(page) + '/?ClickID=1'
    res = requests.get(url)
    html = res.text.replace('\n', '').replace('\t', '').replace(' ', '')
    # 第一次匹配正则
    pat = '<lilogr=".*?"sortid=".*?"><divclass="img_list"><ahref=".*?"tongji_label="listclick"onclick=".*?"' \
          'target="_blank">(.*?)<divclass="listline"></div></li>'
    data = re.findall(pat, html)
    # 第二次匹配正则
    pat3 = '<imglazy_src="//(.*?)".*?<spanclass="(.*?)">.*?</span>.*?target="_blank".*?>(.*?)</a>.*?' \
           '<pclass="roomstrongbox">(.*?)</p>.*?<bclass="strongbox">(.*?)</b>元/月'
    a = 0
    # 以追加写模式打开文件 并以58_+时间戳命名 确保每次抓取数据保存到不同文件中
    text = open('58_' + str(time.time()) + '.txt', mode='a+')
    # 便利获取到的房源信息
    for i in data:
        # 精确获取所需房源信息
        results = re.findall(pat3, i)
        if len(results) == 0:
            # print(i)
            pass
        else:
            if results[0][1] == 'ax-tip':
                pass
            else:
                info = '-' * 20 + '第' + str(a + 1) + '条数据' + '-' * 20
                info1 = '标题:' + results[0][2] + "\n图片链接:" + results[0][0] + "\n户型:" + results[0][3] + "\n价格:" + \
                        results[0][
                            4]
                text.write(info + '\n')
                text.write(info1 + '\n')

                print(info)
                print(info1)
                # print('-' * 20 + '第' + str(a + 1) + '条数据' + '-' * 20)
                a += 1
    info2 = '-' * 20 + '统计信息' + '-' * 20
    info3 = '抓取时间:' + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())) + '\n抓取结果:共抓取' + str(
        len(data)) + "条数据,符合要求数据共计" + str(a) + "条"
    text.write(info2 + '\n')
    text.write(info3 + '\n')
    # 关闭文件对象
    text.close()
    print(info2)
    print(info3)


def get_58s(pages):
    datas = []
    # 如果页数为1 则只进行一次抓取
    if pages == 1:
        url = 'http://bj.58.com/dashanzi/chuzu/pn' + str(pages) + '/?ClickID=1'
        res = requests.get(url)
        html = res.text.replace('\n', '').replace('\t', '').replace(' ', '')
        pat = '<lilogr=".*?"sortid=".*?"><divclass="img_list"><ahref=".*?"tongji_label="listclick"onclick=".*?"' \
              'target="_blank">(.*?)<divclass="listline"></div></li>'
        data = re.findall(pat, html)
        datas = data
    else:
        for i in range(1, pages + 1):
            url = 'http://bj.58.com/dashanzi/chuzu/pn' + str(i) + '/?ClickID=1'
            res = requests.get(url)
            html = res.text.replace('\n', '').replace('\t', '').replace(' ', '')
            pat = '<lilogr=".*?"sortid=".*?"><divclass="img_list"><ahref=".*?"tongji_label="listclick"onclick=".*?"' \
                  'target="_blank">(.*?)<divclass="listline"></div></li>'
            data = re.findall(pat, html)
            # print(html)
            # 将每次获取到的数据数组进行拼接以便后面遍历
            if i == 1:
                datas = data
            else:
                datas += data
    pat3 = '<imglazy_src="//(.*?)".*?<spanclass="(.*?)">.*?</span>.*?target="_blank".*?>(.*?)</a>.*?' \
           '<pclass="roomstrongbox">(.*?)</p>.*?<bclass="strongbox">(.*?)</b>元/月'

    a = 0
    # 以追加写模式打开文件 并以58_+时间戳命名 确保每次抓取数据保存到不同文件中
    text = open('58_' + str(time.time()) + '.txt', mode='a+')
    for i in datas:
        results = re.findall(pat3, i)
        if len(results) == 0:
            # print(i)
            pass
        else:
            if results[0][1] == 'ax-tip':
                pass
            else:

                info = '-' * 20 + '第' + str(a + 1) + '条数据' + '-' * 20
                info1 = '标题:' + results[0][2] + "\n图片链接:" + results[0][0] + "\n户型:" + results[0][3] + "\n价格:" + results[0][
                        4]
                text.write(info+'\n')
                text.write(info1+'\n')

                print(info)
                print(info1)
                # print('-' * 20 + '第' + str(a + 1) + '条数据' + '-' * 20)
                a += 1


    info2 ='-' * 20 + '统计信息' + '-' * 20
    info3 ='抓取时间:'+str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) )+'\n抓取结果:共抓取' + str(len(datas)) + "条数据,符合要求数据共计" + str(a) + "条"
    text.write(info2 + '\n')
    text.write(info3 + '\n')
    # 关闭文件对象
    text.close()
    print(info2)
    print(info3)


if __name__ == '__main__':
    print('-------欢迎使用58同城房屋出租信息抓取程序-------')
    print('因安选房源信息显示异常,故本程序自动过滤安选房源信息')
    while True:
        input_sys = input('请输入操作选项: 1.爬取第N页信息  2.爬取前N页信息  3.退出')
        if input_sys == '1':
            while True:
                page = input('请输入页码(q返回):')
                if page == 'q':
                    break
                if isinstance(int(page), int):
                    print('请稍等,正在获取中...')
                    get_58(int(page))
                else:
                    print('页码输入错误，请输入整数页码')
        elif input_sys == '2':
            while True:
                pages = input('请输入要爬取几页(q返回):')
                if pages == 'q':
                    break
                if isinstance(int(pages), int):
                    print('请稍等,正在获取中...')
                    print('如获取页数过多请耐心等待...')
                    print('如长时间无反应请手动结束程序')
                    get_58s(int(pages))
                else:
                    print('页码输入错误，请输入整数页码')
        elif input_sys == '3':
            print('感谢使用,再见!')
            break
        else:
            print('输入错误，请重新输入')
